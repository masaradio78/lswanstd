const express = require('express')
const serveStatic = require('serve-static')
const history = require('connect-history-api-fallback')
const enforce = require('express-sslify')
const PORT = process.env.PORT || 5000;
const app = express();

if (process.env.ENFORCE_HTTPS) {
  app.use(enforce.HTTPS({ trustProtoHeader: true }));
}

app.use(serveStatic(__dirname + '/dist'));
app.use(history());



app.listen(PORT, () => console.log('Server started on ' + PORT));
