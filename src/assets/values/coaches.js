export const coaches = [
  {
    name: "ab",
    encodedNumbers: "62, 63, 9, 18, 26, 50, 21",
    profileLink: "@/assets/img/ab.jpg"
  },
  {
    name: "pc",
    encodedNumbers: "62, 63, 1, 12, 24, 33, 55",
    profileLink: "@/assets/img/pc.jpg"
  },
  {
    name: "bj",
    encodedNumbers: "62, 63, 3, 40, 13, 34, 31",
    profileLink: "@/assets/img/bj.jpg"
  },
  {
    name: "im",
    encodedNumbers: "510, 511, 81, 260, 273, 69, 495",
    profileLink: "@/assets/img/im.jpg"
  },
  {
    name: "am",
    encodedNumbers: "126, 127, 17, 92, 71, 0, 110",
    profileLink: "@/assets/img/ab.jpg"
  },
  {
    name: "dn",
    encodedNumbers: "1022, 1023, 129, 594, 663, 529, 300",
    profileLink: "@/assets/img/dn.jpg"
  },
  {
    name: "bs",
    encodedNumbers: "62, 63, 33, 5, 0, 40, 58",
    profileLink: "@/assets/img/bs.jpg"
  },
  {
    name: "fy",
    encodedNumbers: "30, 31, 17, 30, 12, 13, 26",
    profileLink: "@/assets/img/fy.jpg"
  }
]
