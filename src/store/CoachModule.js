import {coaches} from "../assets/values/coaches";

export const CoachModule = {
  namespaced: true,
  state: {
    currentCoach: null,
    coaches : []
  },

  getters: {
    currentNumberCoach: state => {
      return state.coaches.indexOf(state.currentCoach) + 1;
    },
    countNoneBeatCoaches: state => {
      return state.coaches.filter(c => c.beat === false).length;
    },
    getIndexCurrentCoach: state => {
      return state.coaches.findIndex(c => state.currentCoach.name === c.name);
    }
  },

  // Mutations are functions hat effect the STATE
  mutations: {
    SET_COACH(state, coach) {
      state.currentCoach = coach;
    },
    SET_NEXT_COACH(state) {
      state.currentCoach = state.coaches[this.getters['Coach/getIndexCurrentCoach'] + 1];
    },
    SET_BEAT_CUR_COACH(state, beat) {
      state.currentCoach.beat = beat;
    },
    SET_COACHES(state) {
      state.coaches = coaches;
      state.coaches.sort(() => Math.random() - 0.5);
      state.coaches.forEach(coach => coach.beat = false);
    },
    SET_COACHES_BEAT_STATUS(state, beat) {
      state.coaches.forEach(coach => coach.beat = beat);
    }
  },

  // Actions are functions that you call throughout your application
  actions: {
    setCoach({commit}, coach) {
      commit('SET_COACH', coach);
    },
    setNextCoach({commit}) {
      commit('SET_NEXT_COACH');
    },
    setBeatCurCoach({commit}, beat) {
      commit('SET_BEAT_CUR_COACH', beat)
    },
    setCoaches({commit}, coaches) {
      commit('SET_COACHES', coaches);
    },
    setCoachesBeatStatus({commit}, beat) {
      commit('SET_COACHES_BEAT_STATUS', beat);
    }
  },
};
