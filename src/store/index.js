import {createStore} from 'vuex'
import {CoachModule} from "./CoachModule";

export default createStore({
  state: {
  },

  getters: {
  },

  // Mutations are functions hat effect the STATE
  mutations: {
  },

  // Actions are functions that you call throughout your application
  actions: {
  },

  modules: {
    Coach: CoachModule
  }
})
