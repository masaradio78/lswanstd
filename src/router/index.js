import {createRouter, createWebHistory} from 'vue-router'
import Home from '../views/Home.vue'
import Result from "../views/Result.vue";
import Game from "../views/Game";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/you_are_the_champion',
    name: 'Result',
    component: Result
  },
  {
    path: '/game',
    name: 'Game',
    component: Game
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
