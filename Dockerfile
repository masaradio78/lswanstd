FROM node:14-alpine AS build

WORKDIR /usr/src/app

COPY . .

RUN npm install
RUN npm run build

FROM nginx:1.17.1-alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist /usr/share/nginx/html

EXPOSE 80
